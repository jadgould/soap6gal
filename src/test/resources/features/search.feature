Feature: Sample

  @ui @pageobject
  Scenario: Should be able to search for a product from the input box
    Given John is viewing the Etsy landing page
    When he searches for a product from the input box
    Then the result should be displayed

  @ui @screenplay
  Scenario: Should be able to search for a product from the input box (screenplay)
    Given John is viewing the Etsy landing page (screenplay)
    When he searches for a product from the input box (screenplay)
    Then the result should be displayed (screenplay)

  @ui @pageobject
  Scenario: Should be able to search for a product from the drop-down menu
	Given John is viewing the Etsy landing page
	When he searches for a product in the drop down menu
	Then the item should be displayed
	
  @ui @pageobject
  Scenario: Should be able to search for a product from the icons
	Given John is viewing the Etsy landing page
	When he searches by product icon
	Then the product should be displayed

  @ui @pageobject
  Scenario Outline: Carousel selection
  	Given John is viewing the Etsy landing page
  	When the <direction> carousel button is tapped
  	Then the next carousel item will be displayed
  	
  Examples:
  |direction|
  |left|
  |right|
  	
  @api @wip @pageobject
  Scenario: Signing In - Valid Sign in
  	Given John is viewing the Etsy Sign In page
  	When John attempts to login with a Valid user name
  	Then the login will be successful
  	
  @api @wip @pageobject
  Scenario: Signing In - Invalid Sign in
  	Given John is viewing the Etsy Sign In page
  	When John attempts to login with a Invalid user name
  	Then the login will not be successful